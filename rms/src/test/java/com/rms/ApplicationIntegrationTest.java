package com.rms;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.annotation.Bean;
import org.springframework.modulith.test.EnableScenarios;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@EnableScenarios
@Testcontainers(disabledWithoutDocker = true)
public class ApplicationIntegrationTest {
    @TestConfiguration
    static class Infrastructure {

        @Bean
        @ServiceConnection
        PostgreSQLContainer<?> database() {
            return new PostgreSQLContainer<>("postgres:15.2");
        }
    }

}
